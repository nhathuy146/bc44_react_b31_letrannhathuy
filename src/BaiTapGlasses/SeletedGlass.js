import React, { Component } from "react";

export default class SeletedGlass extends Component {
  render() {
    const { url, name, desc } = this.props;

    return (
      <div>
        <img
          className="position-absolute"
          src={url}
          alt=""
          style={{
            width: 150,
            top: "45%",
            left: "50%",
            transform: "translate(-50%, -150%)",
            opacity: 0.6,
          }}
        />
        <div
          className="text-left text-white"
          style={{
            position: "absolute",
            top: "80%",
            left: "50%",
            transform: "translate(-50%, -50%)",
            padding: "10px",
            backgroundColor: "rgba(107, 134, 192, 0.5)",
          }}
        >
          <h4>{name}</h4>
          <p>{desc}</p>
        </div>
      </div>
    );
  }
}
