import React, { Component } from "react";
import Glasses from "./Glasses";
import { glassArr } from "./dataGlasses";
import SeletedGlass from "./SeletedGlass";

export default class BaiTapGlasses extends Component {
  state = {
    selectedGlass: null,
  };

  handleGlassSelect = (glass) => {
    this.setState({
      selectedGlass: glass,
    });
  };

  renderGlassList = () => {
    return glassArr.map((glass) => {
      return (
        <Glasses
          key={glass.id}
          data={glass}
          onGlassSelect={this.handleGlassSelect}
        />
      );
    });
  };

  render() {
    const { selectedGlass } = this.state;

    return (
      <div className="container text-center">
        <h1>Try Glass Online</h1>
        <div className="d-flex">
          <div className="position-relative mr-3">
            <img
              style={{
                width: "60%",
              }}
              src="./glassesImage/model.jpg"
              alt="model"
            />
            {selectedGlass && (
              <SeletedGlass
                url={selectedGlass.url}
                name={selectedGlass.name}
                desc={selectedGlass.desc}
              />
            )}
          </div>
          <div>
            <img
              style={{
                width: "60%",
              }}
              src="./glassesImage/model.jpg"
              alt=""
            />
          </div>
        </div>
        <div className="row mt-5">{this.renderGlassList()}</div>
      </div>
    );
  }
}
