import React, { Component } from "react";

export default class Glasses extends Component {
  handleClick = () => {
    const { data, onGlassSelect } = this.props;
    onGlassSelect(data);
  };

  render() {
    const { url } = this.props.data;

    return (
      <div className="col-3 my-1">
        <img
          style={{ width: 150 }}
          src={url}
          alt=""
          onClick={this.handleClick}
        />
      </div>
    );
  }
}
